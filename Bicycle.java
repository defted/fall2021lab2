public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }
    public int getNumberGears() {
        return this.numberGears;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public double getMaxSpeed() {
        return this.maxSpeed;
    }

    public String toString() {
        return "Manufacturer : " + getManufacturer() + "\r\nNumber of gears: " + getNumberGears() + "\r\nMax Speed: " + getMaxSpeed();
    }



}