public class BikeStore {
    public static void main (String[] args) {
        Bicycle[] bike = new Bicycle[4];

        // Creating the bikes
        bike[0] = new Bicycle("Test", 10, 20);
        bike[1] = new Bicycle("Test", 15, 30);
        bike[2] = new Bicycle("Placeholder", 20, 40);
        bike[3] = new Bicycle("Placeholder", 25, 50);

        // Creating a loop
        for (int i = 0; i < bike.length; i++) {
            System.out.println(bike[i]);
        }
     }
    
}
